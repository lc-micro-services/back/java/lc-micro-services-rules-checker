package net.lecousin.microservices.rules;

import org.apache.bcel.classfile.JavaClass;

public interface Rule {
	
	interface ProjectRule extends Rule {

		void checkProject(Project project, CheckerContext ctx);
		
	}
	
	interface ClassRule extends Rule {
		
		void checkClass(Project project, JavaClass clazz, CheckerContext ctx);
		
	}
	
	interface JavaProjectRule extends Rule {
		
		void checkJavaProject(Project project, CheckerContext ctx);
		
	}
	
	interface MultiModuleProjectRule extends Rule {
		
		void checkMultiModuleProject(Project project, CheckerContext ctx);
		
	}
	
}
