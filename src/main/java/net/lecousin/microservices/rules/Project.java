package net.lecousin.microservices.rules;

import java.io.File;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Project {
	
	@Getter
	private final Project parent;
	@Getter
	private final File directory;
	
	public Project getRootProject() {
		if (parent == null)
			return this;
		return parent.getRootProject();
	}
	
	public String getRelativePath() {
		String rootPath = getRootProject().getDirectory().getPath();
		String thisPath = directory.getPath().replace('\\', '/');
		return thisPath.substring(rootPath.length());
	}
	
}
