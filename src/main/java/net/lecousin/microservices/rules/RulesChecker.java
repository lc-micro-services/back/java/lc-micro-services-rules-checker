package net.lecousin.microservices.rules;

import java.util.Arrays;

import org.apache.bcel.util.ClassPath;

public class RulesChecker {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: <path_of_project>");
			System.err.println("Received: " + Arrays.toString(args));
			System.exit(1);
		}
		//String rootDir = args[0];
		try {
			CheckerContext context = new CheckerContext();
			context.setClassPath(ClassPath.getClassPath());
			//Project root = new Project(null, new File(rootDir).getCanonicalFile());
			// TODO
			if (context.getErrors().isEmpty()) {
				System.out.println("No violation of yoko rules found. Good job !");
				return;
			}
			for (CheckerError error : context.getErrors()) {
				System.err.println("[ERROR] " + error.getLocation().getPath() + ": " + error.getMessage());
			}
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Error occurred while checking: " + e.getMessage());
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}
	
}
