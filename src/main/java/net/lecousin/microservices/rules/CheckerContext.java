package net.lecousin.microservices.rules;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import lombok.Data;

@Data
public class CheckerContext {

	private List<Rule> rules;
	private List<CheckerError> errors = new LinkedList<>();
	private String classPath;
	
	public void addError(CheckerError error) {
		this.errors.add(error);
	}
	
	public void addError(File location, String message) {
		addError(new CheckerError(location, message));
	}
	
	public void addError(String location, String message) {
		addError(new File(location), message);
	}
	
	public void addError(File location, String doing, Throwable t) {
		addError(location, "Error " + doing + ": " + t.getMessage());
	}
	
}
